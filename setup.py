from setuptools import setup

setup(name='plirc',
      version='0.1',
      description='An implementation of Lirc in Python',
      url='https://gitlab.com/geri0n/plirc.git',
      author='Gerion Entrup',
      author_email='gerion.entrup@flump.de',
      license='GPLv3',
      packages=['plirc'],
      scripts=['bin/plirc'],
      zip_safe=False,
      install_requires=['pyserial', 'python-daemon'])
