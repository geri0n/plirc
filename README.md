# Plirc, a Python daemon, that speeks the Lirc protocol

Plirc aims to be a full featured, but modular daemon, that speaks the Lirc protocol and that you can use instead of Lirc.
Anyway, Lirc has way more drivers/features/possibilities.

It works with connection arbitrary sources (of events) to arbitrary sinks.

## Minimal example
Plirc can be started with:
```
plirc -c <path/to/configfile>
```

## Config file
Plirc is configured with a config file.
All meta options and all drivers can be configured with a separate section.
The config file can be given per command line flag.
Otherwise it is searched in `~/.config/plirc.conf` and when not found there in `/etc/plirc.conf`.

The meta options are described in the following sections.
For the driver options please refer to the drivers section.

There are some examples of valid config files in the config folder.

### Plirc section
There is one root section called `[plirc]`, that setup the drivers.
The format is the following:
```
sourcename1: sinkname1
sourcename2: sinkname2
```
The names are referring to own sections, that specify the driver options.
In this example it connects `sourcename1` with `sinkname1` and `sourcename2` to `sinkname2`.

### Logging section
This section is named `[logging]`.
It is optional.
If it is not defined, logging to stderr will take place with the command line options or to syslog with level `warning` in daemon mode.

If the section is present, it will define the logging both in normal and in daemon mode.
The following key value pairs are supported:

- `type`: Value is `syslog`, `file` or `stream`. Logs to the syslog, a file or stderr.
- `log_file`: A path to a logfile, only valid with `type: file`. If the path is relative, it is relative to the config directory.
- `level`: Logs with the given level. Possible values are one of `critical`, `error`, `warning`, `info` or `debug`.

### Event grabbing
Plirc supports the executing of arbitrary scripts for different events.
You can define so called 'event grabbing' with a config section `[eg]`.

The following config options are available:

- `actions`: JSON dictionary of all actions, that are available. The key of the dictionary is the name of the key event, the value of the dictionary a list of the name of the script and the user, under whom the script is executed. An example would be `actions: {"KEY_X": ["../test/do_stuff.sh", "leo"]}`. This would run `do_stuff.sh` as user `leo`, when the event `KEY_X` happens.

All scripts are executed with the parameters: 'key number', 'repeat', 'key name' and 'driver name'. If it is possible, root rights are dropped when initializing (in the previous example, right can be dropped from _root_ to _leo_).

See `config/test-eg.conf` for an example of event grabbing and an appropriate script.

## Drivers
The following drivers are available

Sources:

- `fake`: Generate arbitrary fake events.
- `serial`: Read events from a serial device.

Sinks:

- `daemon`: A classic Lirc daemon.

A few config options are available to all sources and sinks:

- `fake_driver`: A source option. Tells plirc (and therefore the lirc clients) another driver name, than the official one.
- `ignore`: A sink option. Input format is a JSON list. Tells all sinks to ignore this events.

The config section of a source or a sink has to contain the `type` key. Its value is the type of the driver, e.g. `serial`.

All paths that are defined in the config are relative to the directory of the config file.

### Source: fake
This source generates a random event every second.
The following config options are available:

- `generate`: Event pool, where events are generated randomly from, defaults to `A`, `B`, `C` and `D`

### Source: serial
This source reads from an serial device (like an Arduino).
The following config options are available:

- `baud`: Baud rate of the device, defaults to 9600.
- `device`: The file, where the device can be found, defaults to '/dev/ttyUSB0'.
- `toggle_bit_mask`: A hexadecimal bitmask, that defines where the toggle bit is, that indicates a new key press. This originates in the RC-5 protocol. Consider `0010` and `1010` are send from the device for multiple presses of `KEY_X`. Then `1000` is the toggle bitmask. It is written as `0x8`.
- `keys`: A JSON dictionary of all keys, that are send with this device. An example would be `{"0x01": "KEY_X", "0x02": KEY_Y}`. If this key is not present, all events are decoded and send as UTF-8.
- `byte_order`: Order of the bytes, that are send. This defaults to `big`. Possible values are `big` and `little`.
- `replacement_key`: The key, that will be send, if the input key is not in the `key` list. Throw an error, if not defined and needed.

### Sink: daemon
Initialize a classic lirc daemon.
The following config options are available:

- `device`: The UNIX socket (a file) of the standard lirc daemon. Defaults to '/var/run/lirc/lircd'.
- `device_links`: A JSON formatted list of file names to symlinks of the `device` option. A common value here is `["/dev/lircd"]`

## Development
You can write drivers by your own. Use always `config/test-dummy.conf` as starting point.
The program can be executed without installing per (assuming, you are in the source folder):
```
PYTHONPATH+='.' ./bin/plirc -c config/test-dummy.conf -vvv
```

### Develop a source
If you want to develop a source, you have to write a subclass of `Source` and link it in the `_SOURCES` list in `plirc/__init__.py`.
Also modify the `test-dummy.conf` file to contain your source. You can see events in the debug messages or per `irw`.

### Develop a sink
If you want to develop a sink, you have to write a subclass of `Sink` and link it in the `_SINK` list in `plirc/__init__.py`.
Also modify the `test-dummy.conf` file to contain your sink. You get events from the `fake` driver.

## License
All code is licensed under the terms of the GNU GPLv3.

## TODO
There are several TODOs:

- Change to UID of the main daemon to a non root user, if it is not needed anymore.
- Implement more of Lirc, especially devinput and network.
- Implement proper shutdown, register signal handler, etc.

## Credit
Thanks to the [Lirc project](https://lirc.org) and [inputlirc](https://github.com/ferdinandhuebner/inputlirc).
