# copyright (c) 2018 Gerion Entrup <gerion.entrup@flump.de>
#
# This file is part of plirc.
#
# plirc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# plirc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with plirc.  If not, see <http://www.gnu.org/licenses/>.

"""Collections of all Exceptions, that are plirc specific."""


class BadConfig(Exception):
    """Configfile is bad."""
