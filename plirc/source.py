# copyright (c) 2018 Gerion Entrup <gerion.entrup@flump.de>
#
# This file is part of plirc.
#
# plirc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# plirc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with plirc.  If not, see <http://www.gnu.org/licenses/>.

"""Implementation of sources, that receive events."""
import selectors
import json
import logging
import time
import random
from multiprocessing import Process, Pipe

import serial

from .node import Node


# pylint: disable=abstract-method
class Source(Node):
    """Provide common functions for a source."""
    def __init__(self, config, directory):
        super().__init__(config, directory)
        self.fake_driver = config.get('fake_driver', None)
        self.sinks = set()

    def __str__(self):
        return f'source.{self.get_type()}'

    def connect(self, sink):
        """Connect a sink with this source."""
        self.sinks.add(sink)

    def send_message(self, key_nr, repeat, key, driver_name=None):
        """Send a message with given arguments to all sources.

        The arguments are the same as in sind.recv_message.

        Arguments:
        key_nr      -- The number of the event (an int).
        repeat      -- Indicates, whether this is a repeated event (a boolean).
        key         -- The translated key (a str).
        driver_name -- The name of the driver, that generated the event.
        """
        if driver_name is None:
            driver_name = self.get_type()
        if self.fake_driver is not None:
            driver_name = self.fake_driver
        for sink in self.sinks:
            sink.recv_message(key_nr, repeat, key, driver_name)


class Fake(Source):
    """Generate random key events for testing purposes."""
    type_name = 'fake'

    def __init__(self, config, directory):
        super().__init__(config, directory)

        # config parsing
        self.generate = config.get('generate', None)
        if self.generate is not None:
            self.generate = json.loads(self.generate)
        else:
            self.generate = ("A", "B", "C", "D")

        self.parent_conn, child_conn = Pipe(False)
        # pylint: disable=invalid-name
        p = Process(target=Fake.fire_events, args=(child_conn, self.generate))
        p.start()

    @staticmethod
    def generate_event(generates):
        """Generate an event."""
        return random.choice(generates)

    @staticmethod
    def fire_events(conn, generates):
        """Put the events into the pipe."""
        logger = logging.getLogger('eg.gen')
        while True:
            event = Fake.generate_event(generates)
            logger.debug('Generated event: %s', event)
            conn.send(event)
            time.sleep(1)
        conn.close()

    def get_selector_action(self):
        return self.parent_conn, selectors.EVENT_READ, self.got_message

    def got_message(self, *_):
        """Got a message from the selector, this means a key was pressed.

        Given arguments are not used.
        """
        message = self.parent_conn.recv()
        self.send_message(1, False, message)


# pylint: disable=too-many-instance-attributes
class Serial(Source):
    """Define a source, that gets its input per serial."""
    type_name = 'serial'

    def __init__(self, config, directory):
        super().__init__(config, directory)

        # config parsing
        self.baud = config.get('baud', 9600)
        self.baud = int(self.baud)
        self.device = config.get('device', '/dev/ttypUSB0')
        self.devide = self.make_abspath(self.device)

        self.toggle_bit_mask = config.get('toggle_bit_mask', None)
        if self.toggle_bit_mask is not None:
            if not self.toggle_bit_mask.startswith('0x'):
                raise Exception("toggle_bit_mask is not hexadecimal")
            self.toggle_bit_mask = int(self.toggle_bit_mask[2:], 16)

        self.keys = config.get('keys', None)
        if self.keys is not None:
            self.keys = json.loads(self.keys)
            temp_keys = {}
            for key, value in self.keys.items():
                if not key.startswith('0x'):
                    raise Exception("Key is not hexadecimal")
                temp_keys[int(key[2:], 16)] = value
            self.keys = temp_keys

        self.byte_order = config.get('byte_order', 'big')
        self.replacement_key = config.get('replacement_key', None)

        # additional attributes
        self.old_toggle = False
        self.serial = serial.Serial(self.device, self.baud)

    def _to_byte_string(self, number):
        """Convert an Integer to an Byte string.

        This is done litte or big endian, according to self.byte_order

        Arguments:
        number -- the number to convert
        """
        return number.to_bytes((number.bit_length() + 1) // 8, self.byte_order)

    def _translate_key(self, key):
        # if no mapping is given, simply decode the string
        if self.keys is None:
            key = self._to_byte_string(key)
            return key.decode('UTF-8', errors='replace')
        # rc5 toggle bit
        if self.toggle_bit_mask is not None:
            key = key & ~self.toggle_bit_mask
        # error handling
        if key not in self.keys:
            if self.replacement_key is None:
                raise Exception(f"Key {key} not defined in config")
            else:
                return self.replacement_key
        return self.keys[key]

    def get_selector_action(self):
        return self.serial, selectors.EVENT_READ, self.got_message

    def got_message(self, *_):
        """Got a message from the selector, this means a key was pressed.

        Given arguments are not used.
        """
        message = self.serial.readline().strip(b'\r\n')
        key_nr = int.from_bytes(message, byteorder=self.byte_order)
        # rc5 toggle bit
        repeat = False
        if self.toggle_bit_mask is not None:
            toggle = (key_nr & self.toggle_bit_mask) > 0
            repeat = (toggle == self.old_toggle)
            self.old_toggle = toggle
        key = self._translate_key(key_nr)
        self.send_message(key_nr, repeat, key)
