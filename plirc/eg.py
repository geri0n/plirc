# copyright (c) 2018 Gerion Entrup <gerion.entrup@flump.de>
#
# This file is part of plirc.
#
# plirc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# plirc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with plirc.  If not, see <http://www.gnu.org/licenses/>.

"""Produce arbitrary scripts, that are defined via config."""
import json
import os
import pwd
import grp
import collections
import subprocess
import logging

from multiprocessing import Process

Script = collections.namedtuple('Script', ['path', 'user'])


class EG(Process):
    """Defines an event grabber."""

    def __init__(self, pipe, section, config_file):
        """Construct an event grabber.

        Arguments:
        pipe        -- receiving end of a multiprocessing.Pipe.
        section     -- config section, that specify the EG.
        config_file -- path of the config file.
        """
        super().__init__()
        self.pipe = pipe
        actions = section.get('actions', {})
        if not isinstance(actions, dict):
            actions = json.loads(actions)
        self.actions = {}
        directory = os.path.dirname(config_file)
        for key, (script, user) in actions.items():
            if not os.path.isabs(script):
                script = os.path.abspath(os.path.join(directory, script))
            self.actions[key] = Script(path=script, user=user)
        self.logger = logging.getLogger('EG')

    @staticmethod
    def get_ids(user, group=None):
        """Return a valid UID and GID (numbers) for given input names.

        Arugments:
        user -- the new user (as string).

        Keyword arguments:
        group -- the new group (as string), defaults to the group of the user.
        """
        pwd_entry = pwd.getpwnam(user)
        uid = pwd_entry.pw_uid
        if group is None:
            gid = pwd_entry.pw_gid
        else:
            gid = grp.getgrnam(group).gr_gid
        return uid, gid

    @staticmethod
    def demote(uid, gid):
        """Change the UID for subprocesses.

        Return a function, that changes UID and GID.
        """
        def result():
            """Helper function, that changes the PID and GID in subprocess."""
            os.setuid(uid)
            os.setgid(gid)
        return result

    @staticmethod
    def is_root(uid):
        """Indicicates whether user is root."""
        return uid == 0

    def run(self):
        self.logger.debug('Starting...')
        if not self.actions:
            self.logger.debug('No actions defined. Quitting...')
            return

        euid = os.geteuid()

        old_user = None
        for action in self.actions.values():
            user = action.user
            # do nothing at first entry
            if old_user is None:
                old_user = user
                continue
            if user != old_user:
                old_user = None
                break
        if old_user is not None:
            uid, gid = EG.get_ids(old_user)
            # if dropping of PIDS is meaningful
            if self.is_root(euid) and euid != uid:
                self.logger.debug(f"Change UID to '{old_user}', since all" +
                                  " scripts are run with this UID.")
                os.setgid(gid)
                os.setuid(uid)
                euid = uid

        while True:
            key_nr, repeat, key, driver = self.pipe.recv()
            if key in self.actions:
                action, user = self.actions[key]
                uid = euid
                if self.is_root(euid):
                    uid, gid = EG.get_ids(user)

                args = [action, str(key_nr), str(repeat), key, driver]
                rargs = ', '.join([f"'{x}'" for x in args[1:]])
                if uid != euid:
                    self.logger.info(f"Executing '{args[0]}' as user" +
                                     f" '{user}' with args: {rargs}")
                    subprocess.Popen(args, preexec_fn=EG.demote(uid, gid))
                else:
                    self.logger.info(f"Executing '{args[0]}'" +
                                     f" with args: {rargs}")
                    subprocess.Popen(args)
                    # TODO better handling of subprocesses
