# copyright (c) 2018 Gerion Entrup <gerion.entrup@flump.de>
#
# This file is part of plirc.
#
# plirc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# plirc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with plirc.  If not, see <http://www.gnu.org/licenses/>.

"""Handle Linux infrared devices."""
import logging
import selectors
import os.path

from multiprocessing import Pipe

from .source import Serial, Fake
from .sink import Daemon, EGSink
from .exceptions import BadConfig
from .eg import EG
from .configuration import Configuration

# all implemented sources
_SOURCES = [Serial, Fake]
# all implemented sinks
_SINKS = [Daemon, EGSink]

_LOGGER = logging.getLogger('plirc.core')

# all used sinks
_SINK_DB = {}
# all implemented sinks
_SOURCE_DB = {}

__all__ = ["Configuration"]


def _setup_obj(section, directory, objs):
    if 'type' not in section:
        raise BadConfig(f"{section.name} defines no type")
    obj = None
    for cand in objs:
        if cand.get_type() == section['type']:
            obj = cand
    if obj is None:
        raise BadConfig(f"{section['type']} is not a valid type.")
    _LOGGER.debug('setup %s', obj)
    return obj(section, directory)


def setup_sink(section, directory):
    """Return a new sink initialized with all options from section.

    Arguments:
    section -- Section of configparser (dict like type).
    """
    return _setup_obj(section, directory, _SINKS)


def setup_source(section, directory):
    """Return a new source initialized with all options from section.

    Arguments:
    section -- Section of configparser (dict like type).
    """
    return _setup_obj(section, directory, _SOURCES)


def setup_eg(section, directory):
    """Setup a new event grabber.

    Arguments:
    section   -- the event grabber config section.
    directory -- the directory, where relative paths start.
    """
    parent, child = Pipe()
    # pylint: disable=invalid-name
    eg = EG(child, section, directory)
    eg.start()
    eg_sink = EGSink(section, parent)
    _SINK_DB['eg'] = eg_sink


def register_from_config(config, directory=None):
    """Register all sinks and sources from a config file.

    Arguments:
    config -- the configparser object

    Keyword arguments
    directory -- directory where relative paths are started from. Defaults to
                 the directory of the config file.
    """
    if 'plirc' not in config:
        raise BadConfig("'plirc' section is not inf config")
    if directory is None:
        directory = os.path.dirname(config.file_name)
    if 'eg' in config:
        setup_eg(config['eg'], directory)
    for source in config['plirc']:
        sinks = config['plirc'][source].split(',')
        if source not in config:
            raise BadConfig(f"'{source}' is defined in 'plirc'" +
                            "but is not defined in config")
        source_obj = setup_source(config[source], directory)
        if 'eg' in config:
            source_obj.connect(_SINK_DB['eg'])
        for sink in sinks:
            if sink in _SINK_DB:
                sink_obj = _SINK_DB[sink]
            else:
                if sink not in config:
                    raise BadConfig(f"'{sink}' is defined in 'plirc'" +
                                    "but is not defined in config")
                sink_obj = setup_sink(config[sink], directory)
                _SINK_DB[sink] = sink_obj
            source_obj.connect(sink_obj)
            _SOURCE_DB[source] = source_obj


def main_loop():
    """Listen forever to new events (with selectors)."""
    sel = selectors.DefaultSelector()
    # get selectors
    for obj in list(_SOURCE_DB.values()) + list(_SINK_DB.values()):
        selector_action = obj.get_selector_action()
        if selector_action is not None:
            _LOGGER.debug(f'Register action for {obj}')
            sel_obj, event, action = selector_action
            sel.register(sel_obj, event, data=action)

    # main loop
    while True:
        events = sel.select()
        for key, _ in events:
            key.data(key.fileobj, sel)
