# copyright (c) 2018 Gerion Entrup <gerion.entrup@flump.de>
#
# This file is part of plirc.
#
# plirc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# plirc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with plirc.  If not, see <http://www.gnu.org/licenses/>.

"""Construct a new sink or source."""
import logging
import os.path


class Node:
    """Common actions for sinks and sources."""
    type_name = 'meta'

    def __init__(self, config, directory):
        """Initialize a Node.

        Arguments:
        config    -- a config section
        directory -- a directory where relative path are started from
        """
        self.name = config.name
        self.logger = logging.getLogger(self.get_type())
        self.directory = directory

    @classmethod
    def get_type(cls):
        """Get the type of the sink or source."""
        return cls.type_name

    def make_abspath(self, path):
        """Make an absolute path out of path."""
        if not os.path.isabs(path):
            path = os.path.abspath(os.path.join(self.directory, path))
        return path

    def get_name(self):
        """Get the actual name of the sink or source."""
        return self.name

    def get_selector_action(self):
        """Get an action for a selector."""
        raise NotImplementedError
