# copyright (c) 2018 Gerion Entrup <gerion.entrup@flump.de>
#
# This file is part of plirc.
#
# plirc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# plirc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with plirc.  If not, see <http://www.gnu.org/licenses/>.

"""Implementation of Sinks, that get events and transmit them somewhere."""
import socket
import selectors
import os
import stat
import json

from .node import Node


class Sink(Node):
    """Base class for sinks.

    A think must have a type_name, that defines the type of sink. See Node for
    more details.
    """

    # pylint: disable=useless-super-delegation
    def __init__(self, config, directory):
        """Initialize a sink with a given config.

        Sink itself is derived from Node, therefore self.init_with_config is
        called upon the config.
        """
        super().__init__(config, directory)
        self.ignore = config.get('ignore', set())
        if not isinstance(self.ignore, set):
            self.ignore = set(json.loads(self.ignore))

    def __str__(self):
        return f'sink.{self.get_type()}'

    def _recv_message(self, key_nr, repeat, key, driver_name):
        raise NotImplementedError

    def recv_message(self, key_nr, repeat, key, driver_name):
        """Every sink must have the ability to receive a message.

        Arguments:
        key_nr      -- The number of the event (an int).
        repeat      -- Indicates, whether this is a repeated event (a boolean).
        key         -- The translated key (a str).
        driver_name -- The name of the driver, that generated the event.
        """
        if key in self.ignore:
            return
        self._recv_message(key_nr, repeat, key, driver_name)


class EGSink(Sink):
    """A pseudo deamon, that forwards all events to the EG process."""
    type_name = 'eg_sink'

    def __init__(self, config, pipe):
        super().__init__(config, None)
        self.pipe = pipe

        self.actions = config.get('actions', set())
        if not isinstance(self.actions, set):
            self.actions = set(json.loads(self.actions).keys())

    def get_selector_action(self):
        """Overrides Node.get_selector_action."""
        return None

    def _recv_message(self, key_nr, repeat, key, driver_name):
        """Overrides Sink._recv_message."""
        if key in self.actions:
            self.pipe.send((key_nr, repeat, key, driver_name))


class Daemon(Sink):
    """A basic socket based lirc daemon such as /dev/lircd."""

    type_name = 'deamon'

    def __init__(self, config, directory):
        """Initialize a Lirc daemon.

        Reads the config first and setup the Unix socket then.
        """
        super().__init__(config, directory)

        # config parsing
        self.device = config.get('device', '/var/run/lirc/lircd')
        self.device = self.make_abspath(self.device)
        self.device_links = config.get('device_links', None)
        if self.device_links is not None:
            self.device_links = json.loads(self.device_links)
            self.device_links = map(self.make_abspath, self.device_links)

        # socket creation
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        # remove the socket, if it exists
        try:
            os.remove(self.device)
        except OSError:
            pass
        # create the directory, if it does not exist
        directory = os.path.dirname(self.device)
        if not os.path.isdir(directory):
            os.makedirs(directory)

        self.sock.bind(self.device)
        # chown 666 to self.device
        os.chmod(self.device, (stat.S_IRUSR | stat.S_IWUSR |
                               stat.S_IRGRP | stat.S_IWGRP |
                               stat.S_IROTH | stat.S_IWOTH))
        # create links of the socket
        if self.device_links is not None:
            # remove all links, if they exist
            for link in self.device_links:
                try:
                    os.remove(link)
                except OSError:
                    pass
                os.symlink(self.device, link)
        self.sock.listen()
        # all clients, that will connect to the socket
        self.clients = set()

    def get_selector_action(self):
        """Overrides Node.get_selector_action."""
        return self.sock, selectors.EVENT_READ, self.got_client

    def got_client(self, _, sel):
        """Callback, when a client connects to the socket."""
        conn, _ = self.sock.accept()
        conn.setblocking(False)
        self.clients.add(conn)
        # the next read is the disconnect, this we only write
        sel.register(conn, selectors.EVENT_READ, self.remove_client)

    def remove_client(self, conn, sel):
        """Callback, when a client disconnects to the socket.

        Arguments:
        conn -- the actual connection on the socket
        sel  -- the selector of all sockets/fileobjects
        """
        sel.unregister(conn)
        conn.close()
        self.clients.remove(conn)

    def _recv_message(self, key_nr, repeat, key, driver_name):
        """Get a message from a source.

        Sends a for Lirc formatted message to all clients, that have connected
        to the socket.

        Overrides Sink._recv_message.
        """
        message = "{:x} {:d} {} {}\n".format(key_nr, repeat, key, driver_name)
        message = message.encode('utf-8')
        self.logger.debug(f"receive message {message}")
        for client in self.clients:
            client.send(message)
